#!/usr/bin/python3

import sys
import calendar
import datetime as dt

"""Simple time tracker written in Python"""


def generateReport(log_path, month):
    """
        Filter log file entries by month
    """
    import re
    with open(log_path, "r") as log_file:
        month_name = calendar.month_name[int(month)]
        file_name = f"timetracker_log_{month_name}.csv"
        with open(file_name, 'w') as month_report:
            lines = log_file.readlines()
            for line in lines:
                if re.search(f"^\d\d\/{month}\/\d\d", line):
                    month_report.write(line)
    return f"Generating report {file_name} for {month_name} from {log_path}"


def displayElapsedTime(time):
    """
        Set display message according to how much time passed
    """
    if time.seconds <= 5400:
        return(f"Total time elapsed: {round(time.seconds / 60)} minutes")
    else:
        return(f"Total time elapsed: {round(time.seconds / 3600)} hours")


def getSummary(project):
    """
        Get project summary after stopping the clock and sanitize it
    """
    return input(
        f"write a summary for what you accomplished with {project}:\n"
    ).strip().replace(',', ' ')


def write_log(file_name, date, start, stop, project):
    """
        Write to log file
    """

    # Sanitize values
    project = project.strip()
    start = start.strftime("%H:%M")
    stop = stop.strftime("%H:%M")
    with open(file_name, "a") as log_file:
        log_file.write(f"{date},{start},{stop},{project},{getSummary(project)}\n")


def validate_month(month):
    """
        Validate month passed to report
    """

    if len(month) == 2:
        return month
    elif len(month) == 1:
        return f"0{month}"
    else:
        print("Month value invalid")
        sys.exit()


# Start script
file_name = "timetracker_log.csv"
if len(sys.argv) > 1:

    if sys.argv[1] == "report":

        # Process report
        month = validate_month(sys.argv[2])
        if len(sys.argv) < 4:
            log_path = "timetracker_log.csv"
        else:
            log_path = sys.argv[3]
        print(generateReport(log_path, month))
        sys.exit()

    else:
        project = sys.argv[1]
else:
    project = input("Please name the project you are going to work on: ")

# Start the counter
start = dt.datetime.now()
print(f"Starting counter, now is {start.strftime('%H:%M:%S')}")

# Wait for user input to stop the clock
out = None
while out is None:
    out = input(f"hit enter to end tracking in project \"{project}\"")

# Stop the clock, get stop time and write to log
else:
    print("stopping...")
    stop = dt.datetime.now()
    date = dt.date.today().strftime("%d/%m/%y")
    print(displayElapsedTime(stop - start))
    write_log(file_name, date, start, stop, project)

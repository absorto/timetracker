## Time tracker ##

This is a very simple time tracker written in Python.
It's purpose is to track the time you spend in a given task. The script creates and
automatically stores your logs in a csv file.

Run it with the command `./timetracker.py` from within the directory it is contained.

The task name is an optional argument that can be provided when calling the script:

`./timetracker.py task`

example:
`./timetracker.py 'Rebuild Website HTML'`

When you finish your task and want to stop your clock, you simply press enter.
The script will prompt you for adding a summary of what you accomplished:

```
stopping...
write a summary for what you accomplished with 'Rebuild Website HTML'
```

This is helpful so you won't forget what this task was all about and have you really
accomplished. What you provide as summary, the task name, the date and the time when 
you started and the time you stopped working on the task will be logged in the csv
file like in this example:

`02/10/18,07:43,07:44,Rebuild Website HTML,added header and body tags`

The script can also filter a monthly report from your logs. By calling the script
with the `report` argument, it will prompt you for the month you want to filter and
the path of the log file:

`./timetracker.py report`

The month must be passed as a two digits number. The report will be saved on the 
working directory as a csv containing only the entries for a given month.
